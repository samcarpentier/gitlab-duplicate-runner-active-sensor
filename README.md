# gitlab-duplicate-runner-active-sensor

Following multiple long-lasting issues due to a runner mistakenly registered twice with the same name, I decided to write a script that will help identify such occurrences.

The principle is very simple: Get all registered runners from GitLab's API and identify the duplicated names.
