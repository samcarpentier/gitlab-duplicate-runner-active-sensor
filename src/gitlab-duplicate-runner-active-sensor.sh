#!/bin/bash

if [ -z $GITLAB_BASE_URL ]; then
  >&2 echo "ERROR: No value provided for [GITLAB_BASE_URL]"
  exit 1
fi

if [ -z $GITLAB_API_TOKEN ]; then
  >&2 echo "ERROR: No value provided for [GITLAB_API_TOKEN]"
  exit 1
fi

if [ -z $DESTINATION_EMAIL_ADDRESS ]; then
  >&2 echo "ERROR: No value provided for [DESTINATION_EMAIL_ADDRESS]"
  exit 1
fi

if [ -z $SOURCE_EMAIL_ADDRESS ]; then
  >&2 echo "ERROR: No value provided for [SOURCE_EMAIL_ADDRESS]"
  exit 1
fi

DEFAULT_RUNNER_NAME_PATTERN=".*"
if [ -z $RUNNER_NAME_PATTERN ]; then
  echo "Using default runner name pattern [${DEFAULT_RUNNER_NAME_PATTERN}]"
  RUNNER_NAME_PATTERN=${DEFAULT_RUNNER_NAME_PATTERN}
fi

DEFAULT_SMTP_SERVER="localhost"
if [ -z $SMTP_SERVER ]; then
  echo "Using default SMTP server [${DEFAULT_SMTP_SERVER}]"
  SMTP_SERVER=${DEFAULT_SMTP_SERVER}
fi

DEFAULT_SMTP_PORT="25"
if [ -z $SMTP_PORT ]; then
  echo "Using default SMTP port [${DEFAULT_SMTP_PORT}]"
  SMTP_PORT=${DEFAULT_SMTP_PORT}
fi


ONLINE_RUNNERS_NAMES=($(curl "${GITLAB_BASE_URL}/api/v4/runners?per_page=100" \
  --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
  --silent | jq '[.[] | select(.status == "online")] | .[].description' | grep -e "${RUNNER_NAME_PATTERN}"))
echo "Found ${#ONLINE_RUNNERS_NAMES[@]} online runners matching the given pattern"

DUPLICATED_RUNNERS_NAMES=($(echo "${ONLINE_RUNNERS_NAMES[@]}" | tr ' ' '\n' | sort | uniq -d))

if [ "${#DUPLICATED_RUNNERS_NAMES[@]}" == 0 ]; then
  echo "No duplicated runners found. You're good to go!"
  exit 0
fi

echo "Found ${#DUPLICATED_RUNNERS_NAMES[@]} runners with at least one duplicate. Sending email alert."


cp alert-template.txt /alert.txt
sed -i "s|%TO%|${DESTINATION_EMAIL_ADDRESS}|g" /alert.txt
sed -i "s|%COUNT%|${#DUPLICATED_RUNNERS_NAMES[@]}|g" /alert.txt
sed -i "s|%INSTANCE%|$GITLAB_BASE_URL|g" /alert.txt

for duplicated_runner in ${DUPLICATED_RUNNERS_NAMES[@]}; do
  echo "  - ${duplicated_runner}" | tr -d '"' >> /alert.txt
done

echo "" >> /alert.txt
echo "You should delete them from the GitLab instance and re-register them." >> /alert.txt
echo "" >> /alert.txt
echo "---" >> /alert.txt
echo "gitlab-duplicate-runner-active-sensor" >> /alert.txt

cat /alert.txt | msmtp \
  --host $SMTP_SERVER \
  --port $SMTP_PORT \
  --from $SOURCE_EMAIL_ADDRESS \
  $DESTINATION_EMAIL_ADDRESS
