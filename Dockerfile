FROM alpine:3.12.2

RUN apk update && apk add bash curl jq msmtp

COPY src/ /

CMD [ "/gitlab-duplicate-runner-active-sensor.sh" ]
